<footer class="page-footer font-small teal pt-4">
    <div class="container-fluid text-center text-md-left">
      <div class="row">
        <div class="col-md-6 mt-md-0 mt-3">
          <h5 class="text-uppercase">
            <?php 
                if(pll_current_language() == 'et') {
                    echo 'Kontakt';
                } else if(pll_current_language() == 'en') {
                    echo 'Contact'; 
                } else if(pll_current_language() == 'ru') {
                    echo 'Контакт'; 
                }  
            ?>
          </h5>
          <p>
            <?php 
                if(pll_current_language() == 'et') {
                    echo 'RANNU Püha Martini kogudus, Valga praostkond
                          <br>Neemisküla, 61109 Elva vald, Tartumaa
                          <br>Tel: +372 55 345 29
                          <br>E-mail: rannu@eelk.ee
                          <br><a href="https://www.facebook.com/rannukirik/" target="_blank">www.facebook.com/rannukirik</a>';
                } else if(pll_current_language() == 'en') {
                    echo 'RANNU Congregation of St. Martin, Valga Prostate
                          <br>Neemisküla, 61109 Elva rural municipality, Tartumaa
                          <br>Mob: +372 55 345 29
                          <br>E-mail: rannu@eelk.ee
                          <br><a href="https://www.facebook.com/rannukirik/" target="_blank">www.facebook.com/rannukirik</a>'; 
                } else if(pll_current_language() == 'ru') {
                    echo 'RANNU Конгрегация Святого Мартина, Валга Простата
                         <br> Neemisküla, 61109 Элва волость, Тартумаа
                         <br> Тел: +372 55 345 29
                         <br> Эл. почта: rannu@eelk.ee
                         <br><a href="https://www.facebook.com/rannukirik/" target="_blank">www.facebook.com/rannukirik</a>'
                }  
            ?>
          </p>
        </div>
        <hr class="clearfix w-100 d-md-none pb-3">
        <div class="col-md-6 mb-md-0 mb-3">
          <h5 class="text-uppercase">
            <?php 
                if(pll_current_language() == 'et') {
                    echo 'Koguduse arvelduskonto';
                } else if(pll_current_language() == 'en') {
                    echo 'Congregation account'; 
                } else if(pll_current_language() == 'ru') {
                    echo 'Счет конгрегации'; 
                }  
            ?>
          </h5>
          <p>
      			<?php 
                if(pll_current_language() == 'et') {
                    echo 'Saaja nimi: EELK Rannu kogudus
                        <br>EE501010102020830009 SEB';
                } else if(pll_current_language() == 'en') {
                    echo 'Recivers name: EELK Rannu kogudus
                        <br>EE501010102020830009 SEB';
                } else if(pll_current_language() == 'ru') {
                    echo 'Имя получателя: EELK Rannu kogudus
                        <br>EE501010102020830009 SEB';
                }  
            ?>
          </p>
        </div>
      </div>
    </div>
  </footer>

<?php wp_footer(); ?>
<script> jQuery(document).ready(function(){ jQuery('.elementor-accordion-item > div').removeClass("elementor-active"); jQuery('.elementor-tab-content').css("display","none"); }); </script>
</body>
</html>