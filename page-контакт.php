<?php include 'contact-form-rus.php'; ?>

<?php get_header(); ?>
	<div class="page-title">
			<h2><?php the_title(); ?></h2>
	</div>
<div class="page-wrapper">
	<div class="page-container">
	<?php while(have_posts()) {
		the_post(); ?>

		<form id="contact" action="<?= $SERVER['PHP_SELF']; ?>" method="post" >
    <h3>Свяжитесь с нами</h3>
    <fieldset>
      <input placeholder="Имя" type="text" tabindex="1" name="thename"  value="<?= $thename ?>" autofocus>
      	<div class="error"><span><?= $name_error ?></span></div>
    </fieldset>
    <fieldset>
      <input placeholder="Емайл" type="text" tabindex="2" name="email" value="<?= $email ?>">
      	<div class="error"><span><?= $email_error ?></span></div>
    </fieldset>
    <fieldset>
      <textarea placeholder="Ваше сообщение.." type="text" tabindex="3" name="message"></textarea>
      	<div class="error"><span><?= $message_error ?></span></div>
    </fieldset>
    <!--<div style="margin-bottom: 15px;" class="g-recaptcha" data-sitekey="6Lcn958UAAAAAKx6kg13pe9c5OOLx8WMrbtWiGv7" name="capcha"></div>-->
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Отправление">Отправить</button>
    </fieldset>
    <div class="success"><?= $success; ?></div>
  </form>
  <!--<script src="https://www.google.com/recaptcha/api.js" async defer></script>-->
		<?php the_content(); ?>
	</div>
</div>

	<?php }

	get_footer(); ?>
	