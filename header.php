<!DOCTYPE html>
<html>
	<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
		<title>Rannu Kogudus - <?php echo the_title(); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
	</head> 	
	<body>		
		        <nav class="main-nav">
              <img src="<?php echo get_bloginfo("template_directory"); ?>/images/logo_monsterrat.png" id="logo"/>
			      <?php
              wp_nav_menu(array(
                'theme_location' => 'headerMenuLocation',
                'container_class' => 'menu1',
                'menu_class'      => 'menu1-items',
                'walker' => new CSS_Menu_Walker()
              ));
            ?>
            <?php
              wp_nav_menu(array(
                'theme_location' => 'mobMenuLocation',
                'container_class' => 'menu_mob',
                'menu_class'      => 'menu_mob-items',
                'walker' => new CSS_Menu_Walker()
              ));
            ?>
            <?php
              wp_nav_menu(array(
                'theme_location' => 'langMenuLocation',
                'container_class' => 'menu2',
                'menu_class'      => 'menu2-items'
              ));
            ?>	
            <?php 
          if ( is_active_sidebar( 'custom-header-widget' ) ) : ?>
                <div id="header-widget-area" class="chw-widget-area widget-area" role="complementary">
                  <?php dynamic_sidebar( 'custom-header-widget' ); ?>
                </div>
                 
        <?php endif; ?>	
		</nav>