<?php
// define variables and set to empty values
$name_error = $email_error = $message_error = "";
$thename = $email = $message = $success = "";
// form is submitted with POST method

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if (empty($_POST["thename"])) {
		$name_error = "Palun sisesta nimi";
	} else {
		$thename = test_input($_POST["thename"]);
		// check if name only contains letters, whitespace and hyphen
		if (!preg_match("/^[a-zA-Z -]*$/",$thename)) {
			$name_error = "Sisestada saab ainult tähti, tühikuid ja sidekriipse";
		}
	}

	if (empty($_POST["email"])) {
	  $email_error = "Palun sisesta email";
	} else {
		$email = test_input($_POST["email"]);
		// email validation
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$email_error = "Sisesta email korrektselt";
		}
	}

	if (empty($_POST["message"])) {
		$message_error = "Palun sisesta sõnum";
	} else {
		$message = test_input($_POST["message"]);
	}

   	if ($name_error == '' and $email_error == '' and $message_error == '' ){
		$message_body = '';
		unset($_POST['submit']);
		foreach ($_POST as $key => $value){
			$message_body .= "$key: $value\n";
		}


		$to = 'ilona.kolossova@khk.ee';
		$subject = 'RANNU KIRIK';
		$message = "Sulle saadeti kiri Rannu koguduse kodulehelt.\n\nSaatja nimi: $thename\n\nSaatja email: $email\n\nSõnum: $message";
		// create email headers
		$headers =  'From: '.$email."\r\n".
					'Reply-To: '.$email."\r\n" .
					'X-Mailer: PHP/' . phpversion();

		if (mail($to, $subject, $message, $headers)){
			$success = "Aitäh, kiri edukalt saadetud! Vastame esimesel võimalusel.";
			$thename = $email = $message = $headers = '';
		}	
	}
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}