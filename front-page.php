<?php get_header(); ?>
<div class="index-container">
	<?php while(have_posts()) {
		the_post(); ?>
	<div class="front-text-container">
		<div class="text-background">

			<?php
				$homepagePost = new WP_Query(array(
	              'posts_per_page' => 1
	        ));

	        while ($homepagePost->have_posts()) {
            	$homepagePost->the_post(); ?>

			<h1>
				<?php the_title(); ?>
			</h1>
                 <p style="font-size: 1.2rem;"><?php echo wp_trim_words(get_the_content(), 50); ?>... <br>
                 	<div id="loe-edasi-avaleht"><a href="<?php the_permalink(); ?>">

                 		<?php 
	                if(pll_current_language() == 'et') {
	                    echo 'Ava artikkel';
	                } else if(pll_current_language() == 'en') {
	                    echo 'Open article'; 
	                } else if(pll_current_language() == 'ru') {
	                    echo 'Открыть статью'; 
	                }  
            	?>
                 		
                 	</a></div>
                 </p>
          <?php } wp_reset_postdata(); ?>

		</div>
	</div>
</div>
	<?php }

	get_footer();

?>