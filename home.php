<title>Uudised</title>
<?php get_header(); ?>

<div class="page-title">
			<h2><?php single_post_title(); ?></h2>
</div>
	<div class="page-wrapper">
		
		<?php get_sidebar( 'primary' ); ?>

		<div class="page-container2">
	<?php while(have_posts()) {
		the_post(); ?>
		
	<div class="post-content">
		<div class="posts-title">
			<h2><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></h2>
			<div class="publish-date">Lisatud: <?php the_time('m. F Y'); ?></div>
		</div>
			
			<div class="fea_img"><?php the_post_thumbnail(); ?></div>

			<p><?php echo wp_trim_words(get_the_content(), 140); ?>...</p>
				<div id="loe-edasi">
					<a href="<?php the_permalink(); ?>" target="_blank">
						<?php 
	                if(pll_current_language() == 'et') {
	                    echo 'Ava artikkel';
	                } else if(pll_current_language() == 'en') {
	                    echo 'Open article'; 
	                } else if(pll_current_language() == 'ru') {
	                    echo 'Открыть статью'; 
	                }  
            	?>
					</a>
				</div>
	</div>
	<br>

	<?php } ?>
	
	<div class="num-pagination">
    	<?php echo paginate_links(); ?>	
	<hr>
	</div>

  </div>
</div>
	<?php get_footer(); ?>